package net.technicaltest.note.service;

import java.util.List;

import net.technicaltest.note.dao.NoteDAO;
import net.technicaltest.note.form.Note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NoteServiceImpl implements NoteService {

	@Autowired
	private NoteDAO noteDAO;
	
	@Transactional
	public void addNote(Note note) {
		noteDAO.addNote(note);
	}

	@Transactional
	public List<Note> listNote() {

		return noteDAO.listNote();
	}

	@Transactional
	public void removeNote(Integer id) {
		noteDAO.removeNote(id);
	}
}
