package net.technicaltest.note.service;

import java.util.List;

import net.technicaltest.note.form.Note;

public interface NoteService {

	public void addNote(Note note);
	public List<Note> listNote();
	public void removeNote(Integer id);
}
