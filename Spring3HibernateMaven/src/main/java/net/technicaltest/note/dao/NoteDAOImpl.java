package net.technicaltest.note.dao;

import java.util.List;

import net.technicaltest.note.form.Note;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NoteDAOImpl implements NoteDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void addNote(Note note) {
		sessionFactory.getCurrentSession().save(note);
	}

	public List<Note> listNote() {

		return sessionFactory.getCurrentSession().createQuery("from Note")
				.list();
	}

	public void removeNote(Integer id) {
		Note note = (Note) sessionFactory.getCurrentSession().load(
				Note.class, id);
		if (null != note) {
			sessionFactory.getCurrentSession().delete(note);
		}

	}

}
