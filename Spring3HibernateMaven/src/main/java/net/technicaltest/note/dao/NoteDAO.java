package net.technicaltest.note.dao;

import java.util.List;

import net.technicaltest.note.form.Note;

public interface NoteDAO {

	public void addNote(Note note);
	public List<Note> listNote();
	public void removeNote(Integer id);
}
