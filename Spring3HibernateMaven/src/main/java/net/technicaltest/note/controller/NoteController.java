package net.technicaltest.note.controller;

import java.util.Map;

import net.technicaltest.note.form.Note;
import net.technicaltest.note.service.NoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class NoteController {

	@Autowired
	private NoteService noteService;

	@RequestMapping("/index")
	public String listNotes(Map<String, Object> map) {

		map.put("note", new Note());
		map.put("noteList", noteService.listNote());

		return "note";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addNote(@ModelAttribute("note")
	Note note, BindingResult result) {

		noteService.addNote(note);

		return "redirect:/index";
	}

	@RequestMapping("/delete/{noteId}")
	public String deleteContact(@PathVariable("noteId")
	Integer noteId) {

		noteService.removeNote(noteId);

		return "redirect:/index";
	}
	
	
}
