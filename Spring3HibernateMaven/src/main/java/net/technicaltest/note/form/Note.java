package net.technicaltest.note.form;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author KondaReddy
 *
 */
@Entity
@Table(name="NOTE")
public class Note {
	@Id
	@Column(name="ID")
	@SequenceGenerator(name="note_seq", sequenceName="note_seq", allocationSize=1, initialValue=1) 
	@GeneratedValue (strategy = GenerationType.SEQUENCE, generator="note_seq")
	private Integer id;
	
	@Column(name="NOTENAME")
	private String notename;
	
	@Column(name="NOTEDESCRIPTION")
	private String notedescription;
	
	@Column(name="AUTHORNAME")
	private String authorname;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="ADDRESS")
	private String address;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNotename() {
		return notename;
	}
	public void setNotename(String notename) {
		this.notename = notename;
	}
	public String getNotedescription() {
		return notedescription;
	}
	public void setNotedescription(String notedescription) {
		this.notedescription = notedescription;
	}
	public String getAuthorname() {
		return authorname;
	}
	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}