<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Spring 3 MVC Series - Note</title>
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.data, .data td {
			border-collapse: collapse;
			width: 100%;
			border: 1px solid #aaa;
			margin: 2px;
			padding: 2px;
		}
		.data th {
			font-weight: bold;
			background-color: #5C82FF;
			color: white;
		}
	</style>
</head>
<body>

<h2>Note Application</h2>

<form:form method="post" action="add.html" commandName="note">

	<table>
	<tr>
		<td><form:label path="notename"><spring:message code="label.notename"/></form:label></td>
		<td><form:input path="notename" /></td> 
	</tr>
	<tr>
		<td><form:label path="notedescription"><spring:message code="label.notedescription"/></form:label></td>
		<td><form:input path="notedescription" /></td>
	</tr>
	<tr>
		<td><form:label path="authorname"><spring:message code="label.authorname"/></form:label></td>
		<td><form:input path="authorname" /></td>
	</tr>
	<tr>
		<td><form:label path="email"><spring:message code="label.email"/></form:label></td>
		<td><form:input path="email" /></td>
	</tr>
	<tr>
		<td><form:label path="address"><spring:message code="label.address"/></form:label></td>
		<td><form:input path="address" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="<spring:message code="label.addnote"/>"/>
		</td>
	</tr>
</table>	
</form:form>

	
<h3>Notes</h3>
<c:if  test="${!empty noteList}">
<table class="data">
<tr>
	<th>Note Description</th>
	<th>Note Name</th>
	<th>Author Name</th>
	<th>Email</th>
	<th>Address</th>
	<th>&nbsp;</th>
</tr>
<c:forEach items="${noteList}" var="note">
	<tr>
		<td>${note.notedescription}</td>
		<td>${note.notename}</td>
		<td>${note.authorname}</td>
		<td>${note.email}</td>
		<td>${note.address}</td>
		<td><a href="delete/${note.id}">delete</a></td>
	</tr>
</c:forEach>
</table>
</c:if>

</body>
</html>
